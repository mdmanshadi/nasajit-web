// adding sticky nav to website
window.onscroll = function() {scrollFunction()};

var rightStickyNav = document.getElementById("right-nav-sticky");
var leftStickyNav = document.getElementById("left-nav-sticky");

var sticky = rightStickyNav.offsetTop + 190;

function scrollFunction() {
    if (window.pageYOffset >= sticky) {
        rightStickyNav.classList.add("sticky");
        leftStickyNav.classList.add("sticky");
    } else {
        rightStickyNav.classList.remove("sticky");
        leftStickyNav.classList.remove("sticky");
    }
}


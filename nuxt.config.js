export default {
    mode: 'universal',
    /*
    ** Headers of the page
    */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'نساجیت - بازار آنلاین پارچه مبلی'},
            {hid: 'author', name: 'author', content: 'نساجیت - بازار آنلاین پارچه مبلی'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'stylesheet', type: 'text/css', href: '/assets/css/bootstrap.min.css'},
            {rel: 'stylesheet', type: 'text/css', href: '/assets/css/bootstrap-rtl.css'},
            {rel: 'stylesheet', type: 'text/css', href: '/assets/css/style.css'},
            {rel: 'stylesheet', type: 'text/css', href: '/assets/css/all.min.css'},
        ],
        script: [
            {src: '/assets/js/jquery.min.js', body: true},
            {src: '/assets/js/popper.min.js', body: true},
            {src: '/assets/js/bootstrap.min.js', body: true},
            {src: '/assets/js/script.js', body: true},
            // {src: '/assets/js/raychat.js', ssr: false},
        ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: {
        color: '#fff',
        height: '10px',
        rtl: true,
    },
    /*
    ** Global CSS
    */
    css: [],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {src: '~plugins/vue-infinite-scroll.js', ssr: false},
        '~/plugins/vuelidate',
        '~/plugins/vue-notification',
        {src: '~/plugins/vue-carousel.js', ssr: false},
        {src: '~/plugins/vue-easy-lightbox.js', ssr: false}
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [],
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
        ['@nuxtjs/google-analytics', {
            id: 'UA-145237031-2'
        }],
        ['@netsells/nuxt-hotjar', {
            id: '1462537',
        }],
        'nuxt-device-detect',
        ['@nuxtjs/pwa', {icon: false}],
    ],
    /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
    axios: {
        baseURL: 'https://nasajit-laravel.liara.run/api/'
    },
    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    },

    /*
    ** PWA configs
    */
    manifest: {
        name: 'نساجیت',
        description: 'بازار آنلاین پارچه مبلی'
    }
}

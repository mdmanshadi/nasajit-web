import Vue from 'vue';
export default function (context) {
    if (!context.store.getters['auth/isAuth']) {
        Vue.notify({
            group: 'alert',
            type: 'error',
            text: 'برای دسترسی به این بخش باید وارد حساب کاربری خود شوید',
        });
        context.redirect('/account/login');
    }
}

import Vue from 'vue';

export const state = () => ({
    cartDetails: null
});
export const mutations = {
    changeProductCount(state, data) {
        state.cartDetails.items.forEach((el) => {
            if (el.id === data.id) {
                if (data.type === 'add') {
                    el.count++;
                } else {
                    if (el.count > 1)
                        el.count--;
                }
            }
        })
    },
    setCartDetails(state, data) {
        state.cartDetails = data;
    },
    updateCart(state, data) {
        state.cartDetails.items.forEach((el) => {
            if (el.id === data.id) {
                if (data.type === 'add') {
                    el.quantity++;
                }
                if (data.type === 'minus') {
                    if (el.quantity > 1) {
                        el.quantity--;
                    }
                }
            }
        })
    }
};
export const getters = {
    getCartTotalPrice(state) {
        if(state.cartDetails === null){
            return '0 تومان';
        }
        let totalPrice = 0;
        state.cartDetails.items.forEach((el)=>{
            totalPrice += parseFloat(el.price) * el.quantity;
        });
        return totalPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' تومان';
    },
    getCartProducts(state) {
        return state.cartDetails === null ? 0 : state.cartDetails.items;
    }
};
export const actions = {
    addToCart({state, rootState, commit}, data) {
        if(rootState.auth.token === null){
            Vue.notify({
                group: 'alert',
                type: 'error',
                text: 'برای دسترسی به این بخش باید وارد حساب کاربری خود شوید',
            });
            return;
        }
        this.$axios.$post('checkout/cart/add/' + data.id, {
            token: rootState.auth.token,
            product: `${data.id}`,
            quantity: 1
        })
            .then((response) => {
                commit('setCartDetails', response.data);
                Vue.notify({
                    group: 'alert',
                    type: 'success',
                    text: 'محصول با موفقیت به سبد خرید شما اضافه شد'
                });
            })
    },
    removeFromCart({state, rootState, commit}, data) {
        this.$axios.get(`/checkout/cart/remove-item/${data.id}`, {
            params: {
                token: rootState.auth.token
            }
        })
            .then((response) => {
                commit('setCartDetails', response.data.data);
                Vue.notify({
                    group: 'alert',
                    type: 'warning',
                    text: 'محصول با موفقیت از سبد خرید شما حذف گردید'
                })
            })
    },
    updateCart({state, rootState, commit}) {
        let cartProductsQuantity = {};
        state.cartDetails.items.forEach((el) => {
            cartProductsQuantity[[`${el.id}`]] = el.quantity
        });
        this.$axios.$put('checkout/cart/update', {
            token: rootState.auth.token,
            qty: cartProductsQuantity
        }).then(response => {
            commit('setCartDetails', response.data);
        });
    },
    saveOrder({rootState, commit, dispatch}) {
        dispatch('updateCart');
        let saveAddressPayload = {
            billing: {
                address1: ['ایران'],
                use_for_shipping: true,
                first_name: rootState.auth.userData.first_name,
                last_name: rootState.auth.userData.last_name,
                city: 'یزد',
                email: 'email@email.com',
                phone: rootState.auth.userData.phone,
                postcode: '8917863539',
                state: 'یزد',
                country: 'IR',
            },
            shipping: {
                address1: ['']
            }
        };
        if (rootState.auth.token != null) {
            saveAddressPayload.token = rootState.auth.token;
        }
        this.$axios.$post('checkout/save-address', saveAddressPayload).then(response => {
            this.$axios.$post('checkout/save-shipping', {
                token: rootState.auth.token,
                shipping_method: "free_free"
            }).then(response => {
                this.$axios.$post('checkout/save-payment', {
                    token: rootState.auth.token,
                    payment: {
                        method: "cashondelivery"
                    }
                }).then(response => {
                    this.$axios.$post('checkout/save-order', {
                        token: rootState.auth.token
                    }).then((response) => {
                        Vue.notify({
                            group: 'alert',
                            type: 'success',
                            text: `سفارش شما با موفقیت با شماره ${response.order.id}  ثبت شد.کارشناسان فروش بلافاصله با شما تماس خواهند گرفت`,
                            duration: 5000
                        });
                        commit('setCartDetails', null);
                    })
                })
            })
        })
    },
    getCartDetails({rootState, commit}) {
        this.$axios.$get('/checkout/cart', {
            params: {
                token: rootState.auth.token
            }
        }).then((response) => {
            commit('setCartDetails', response.data);
        })
    }
};


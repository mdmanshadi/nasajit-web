import Vue from 'vue';

export const state = () => ({
    products: null,
    newProducts: null,
    favoriteProducts: null
});

export const mutations = {
    setProducts(state, data) {
        state.products = data.data;
    },
    setNewProducts(state, data) {
        state.newProducts = data.data
    },
    setFeaturedProducts(state, data) {
        state.featuredProducts = data.data
    },
    setLoadedProducts(state, data) {
        state.products.push(...data.data);
    },
    setFavorites(state, data) {
        state.favoriteProducts = data.data;
    }
};

export const actions = {
    addToFavorites({dispatch, state, rootState}, product_id) {
        this.$axios.$get('wishlist/add/' + product_id, {
            params: {
                token: rootState.auth.token
            }
        }).then(() => {
            Vue.notify({
                group: 'alert',
                type: 'success',
                text: 'محصول با موفقیت به علاقه مندی ها اضافه شد'
            });
        });
    },
    getFavorites({commit, rootState}) {
        return this.$axios.$get('wishlist', {
            params: {
                token: rootState.auth.token,
                customer_id: rootState.auth.userData.id
            }
        })
            .then((response) => {
                return commit('setFavorites', response)
            })
    },
    removeFromFavorites({dispatch, state, rootState}, product_id) {
        this.$axios.$get('wishlist/add/' + product_id, {
            params: {
                token: rootState.auth.token
            }
        }).then(() => {
            dispatch('getFavorites');
            Vue.notify({
                group: 'alert',
                type: 'error',
                text: 'محصول با موفقیت از لیست علاقه مندی ها حذف شد'
            });
        });
    },
};
import Vue from 'vue';
import Cookie from 'js-cookie'

export const state = () => ({
    token: null,
    userData: null,
    orders: null
});

export const mutations = {
    setToken(state, data) {
        state.token = data.token;
    },
    setUserData(state, data) {
        state.userData = data.data;
    },
    setOrders(state, data) {
        state.orders = data.data;
    }
};
export const actions = {
    initAuth(vuexContext, req) {
        let token;
        if (req) {
            if (!req.headers.cookie) {
                return;
            }
            const jwtCookie = req.headers.cookie
                .split(";")
                .find(c => c.trim().startsWith("jwt="));
            if (!jwtCookie) {
                return;
            }
            token = jwtCookie.split("=")[1];
        } else {
            token = localStorage.getItem("token");
        }
        vuexContext.commit("setToken", {token: token});
    },
    registerRequest({commit}, data) {
        this.$axios.$post('customer/register', {
            first_name: data.firstName,
            last_name: data.lastName,
            phone: data.phone,
            password: data.password,
            password_confirmation: data.repeatPassword,
        })
            .then((response) => {
                    Vue.notify({
                        group: 'alert',
                        type: 'success',
                        text: 'شما با موفقیت عضو شدید. هم اکنون می توانید وارد سایت شوید'
                    });
                    this.$router.replace('/account/login');
                }
            ).catch(() => {
            Vue.notify({
                group: 'alert',
                type: 'warning',
                text: 'در هنگام عضویت مشکلی رخ داد'
            });
            this.$router.replace('/account/register');
        })
    },
    loginRequest(context, data) {
        this.$axios.$post('customer/login', {
            phone: data.phone,
            password: data.password,
            token: true
        }).then((response) => {
                context.commit('setToken', response);
                context.commit('setUserData', response);
                context.dispatch('cart/getCartDetails', null, {root: true});
                localStorage.setItem('token', response.token);
                Cookie.set("jwt", response.token, {expires: 90});
                Vue.notify({
                    group: 'alert',
                    type: 'success',
                    text: 'شما با موفقیت وارد شدید'
                });
                this.$router.replace('/');
            }
        ).catch(() => {
            Vue.notify({
                group: 'alert',
                type: 'error',
                text: 'شماره موبایل یا رمز عبور شما اشتباه است. مجددا تلاش کنید'
            });
            this.$router.replace('/account/login');
        })
    },
    logOut({state, rootState, commit}) {
        commit('setToken', {
            token: null
        });
        commit('setUserData', {
            data: null
        });
        commit('cart/setCartDetails',null,{root: true});
        commit('products/setFavorites',{data:''},{root: true});
        localStorage.removeItem('token');
        Cookie.remove('jwt');
        this.$router.replace('/account/login');
        Vue.notify({
            group: 'alert',
            type: 'warning',
            text: 'شما با موفقیت خارج شدید',
        });
    },
    getUserData({commit,rootState}){
        this.$axios.$get('customer/get', {
            params:{
                token: rootState.auth.token
            }
        }).then((response)=>{
            commit('setUserData',response);
        });
    }
};
export const getters = {
    isAuth(state) {
        return state.token ? true : false;
    },
};
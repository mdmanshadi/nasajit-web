export const sales = [
    {
        id: 1,
        title: 'گروه پارچه مبلی فلوریس',
        description: 'تمامی پارچه های این خانواده با تخفیف به فروش می رسد',
        discount: '15%',
        price:'88,600 تومان',
        saleFormattedPrice:'75,300 تومان',
        image:'https://nasajit-laravel.liara.run/cache/medium/product/370/eZN8eLxpBLy7hboV6Cc7Kk5JR6vpSZgs4kbeUJ77.jpeg',
        route: '/products/search?query=فلوریس'
    },
    {
        id: 5,
        title: 'گروه پارچه مبلی سایا',
        description: 'تمامی پارچه های این خانواده با تخفیف به فروش می رسد',
        discount: '15%',
        price:'88,600 تومان',
        saleFormattedPrice:'75,300 تومان',
        image:'https://nasajit-laravel.liara.run/cache/medium/product/319/SGhp8gZlmOFADsUirwuW2urnMwSGqF7JPXnMLjgX.jpeg',
        route: '/products/search?query=سایا'
    },
    {
        id: 6,
        title: 'گروه پارچه مبلی هافمن',
        description: 'در صورت ثبت سفارش طاقه قیمت هر متر 95 هزار تومان محاسبه می شود',
        discount: '18%',
        price:'132,900 تومان',
        saleFormattedPrice:'109,000 تومان',
        image:'https://nasajit-laravel.liara.run/cache/medium/product/122/54D9wbaiVfivTCff0gsxUnb2jQXN8hDoWQtxh3MF.jpeg',
        route: '/products/search?query=هافمن'
    },
    {
        id: 7,
        title: 'گروه پارچه مبلی ویکتور',
        description: 'در صورت ثبت سفارش طاقه قیمت هر متر 64 هزار تومان محاسبه می شود',
        discount: '15%',
        price:'88,600 تومان',
        saleFormattedPrice:'75,300 تومان',
        image:'https://nasajit-laravel.liara.run/cache/medium/product/191/6aBNnoiCKMVXz0sw2Z8nXaZbNz7hF8Q4uDMi3htR.jpeg',
        route: '/products/search?query=ویکتور'
    },
];
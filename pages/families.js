export const families = [
    {
        title: 'گروه پارچه مبلی آنیا',
        image:'/assets/images/product-families/aniya.jpg'
    },
    {
        title: 'گروه پارچه مبلی فلوریس',
        image:'/assets/images/product-families/floris.jpg'
    },
    {
        title: 'گروه پارچه مبلی سالویا',
        image:'/assets/images/product-families/salvia.jpg'
    },
    {
        title: 'گروه پارچه مبلی ویانا',
        image:'/assets/images/product-families/viyana.jpg'
    },
    {
        title: 'گروه پارچه مبلی آرمیس',
        image:'/assets/images/product-families/armis.jpg'
    },
    {
        title: 'گروه پارچه مبلی روبینا',
        image:'/assets/images/product-families/robina.jpg'
    },
    {
        title: 'گروه پارچه مبلی ریتا',
        image:'/assets/images/product-families/rita.jpg'
    },
    {
        title: 'گروه پارچه مبلی ساوالان',
        image:'/assets/images/product-families/savalan.jpg'
    },
    {
        title: 'گروه پارچه مبلی رانیکا',
        image:'/assets/images/product-families/ranika.jpg'
    },
    {
        title: 'گروه پارچه مبلی نیتا',
        image:'/assets/images/product-families/nita.jpg'
    },
    {
        title: 'گروه پارچه مبلی ملیتا',
        image:'/assets/images/product-families/melita.jpg'
    },
    {
        title: 'گروه پارچه مبلی فریا',
        image:'/assets/images/product-families/feria.jpg'
    },
    {
        title: 'گروه پارچه مبلی سایا',
        image:'/assets/images/product-families/saya.jpg'
    },
    {
        title: 'گروه پارچه مبلی آلما',
        image:'/assets/images/product-families/alma.jpg'
    },
    {
        title: 'گروه پارچه مبلی آنیا',
        image:'/assets/images/product-families/ania.jpg'
    },
    {
        title: 'گروه پارچه مبلی آیلار',
        image:'/assets/images/product-families/aylar.jpg'
    },
    {
        title: 'گروه پارچه مبلی باسکال',
        image:'/assets/images/product-families/bascal.jpg'
    },
    {
        title: 'گروه پارچه مبلی دنیز',
        image:'/assets/images/product-families/deniz.jpg'
    },
    {
        title: 'گروه پارچه مبلی فریتا',
        image:'/assets/images/product-families/ferita.jpg'
    },
    {
        title: 'گروه پارچه مبلی هافمن',
        image:'/assets/images/product-families/hafman.jpg'
    },
    {
        title: 'گروه پارچه مبلی جیوانجی',
        image:'/assets/images/product-families/jivanji.jpg'
    },
    {
        title: 'گروه پارچه مبلی لایکو',
        image:'/assets/images/product-families/layko.jpg'
    },
    {
        title: 'گروه پارچه مبلی مینوسا',
        image:'/assets/images/product-families/minoosa.jpg'
    },
    {
        title: 'گروه پارچه مبلی پاناما',
        image:'/assets/images/product-families/panama.jpg'
    },
    {
        title: 'گروه پارچه مبلی سناتور',
        image:'/assets/images/product-families/senator.jpg'
    },
    {
        title: 'گروه پارچه مبلی سیلوانا',
        image:'/assets/images/product-families/silvana.jpg'
    },
    {
        title: 'گروه پارچه مبلی تانسو',
        image:'/assets/images/product-families/tanso.jpg'
    },
    {
        title: 'گروه پارچه مبلی ویکتور',
        image:'/assets/images/product-families/victor.jpg'
    },
    {
        title: 'گروه پارچه مبلی ویلسون',
        image:'/assets/images/product-families/wilson.jpg'
    },
    {
        title: 'گروه پارچه مبلی ژربرا',
        image:'/assets/images/product-families/zherbra.jpg'
    }
];